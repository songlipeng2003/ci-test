package test;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UtilTest {

    @Test
    void hello() {
        String result = Util.hello("slp");

        assertEquals("hello slp", result);
    }
}